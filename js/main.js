const $ = window.$;

$('.toggle-menu').click( function () {
	$('body').toggleClass('main-menu-open');
})

$('.main-search').keyup( function() {
	var mainSearchValue = this.value;

	if (mainSearchValue != '') {
		$('.input-value').html(mainSearchValue);
		$('.jump-to-suggestions').removeClass('d-none');
	}
	else {
		$('.jump-to-suggestions').addClass('d-none');
	}
});

var elementPosition = $('#navegacao-lateral').offset();
var topNav = $('#top-nav .position-fixed').outerHeight() + 20;

$(document).on("scroll", onScroll);

$(document).ready( function() {

	//smoothscroll
	$('#navegacao-lateral a[href^="#"]').on('click', function (e) {
		// e.preventDefault();

		$(document).off("scroll");

		$('a').each(function () {
			$(this).removeClass('active');
			$(this).removeClass('item-ativo');
		})

		$(this).addClass('active');
		$(this).addClass('item-ativo');

		var target = this.hash,
			menu = target;
		$target = $(target);

		$('html, body').unbind().animate(
			{'scrollTop': $target.offset().top - 80}, 500, 'swing', 
			function () {
				window.location.hash = target;
				$(document).on("scroll", onScroll);
			}
		);
	});

	// onScroll();
});


function onScroll(event){
    var scrollPos = $(document).scrollTop();

    if ($(window).scrollTop() > elementPosition.top - topNav) {
        $('#navegacao-lateral').css('position', 'fixed').css('top', topNav);
    } else {
        $('#navegacao-lateral').css('position', 'static');
    }

    $('#navegacao-lateral a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#navegacao-lateral a').removeClass("active");
            currLink.addClass("active");
            currLink.addClass('item-ativo');

        }
        else{
            currLink.removeClass("active");
            currLink.removeClass('item-ativo');

        }
    });
}

// $(window).scroll(function(){

// 	var chapterContent = $('#chapter-content');
// 	var topNav = $('#top-nav .position-fixed');
// 	var secondNav = $('#top-fixed-bar');
// 	var topNavsHeight = secondNav.outerHeight();
//   	var scroll = $(window).scrollTop();

// 	if (scroll > topNavsHeight) {
// 		chapterContent.addClass('fixed').css( 'top', topNav.outerHeight() );
// 	}

// 	else chapterContent.removeClass('fixed').css( 'top', '0' );

// });